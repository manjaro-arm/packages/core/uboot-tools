# Maintainer: Dan Johansen <strit@manjaro.org>
# Contributor: Kevin Mihelich <kevin@archlinuxarm.org>
# Contributor: Jonny Gerold <jonny@fsk141.com>
# Contributor: Julian Langschaedel <meta.rb@gmail.com>
# Contributor: Dragan Simic <dsimic@buserror.io>

pkgname=uboot-tools
pkgver=2023.01
pkgrel=1
pkgdesc="U-Boot tools"
arch=('arm' 'armv6h' 'armv7h' 'aarch64')
license=('GPL' 'LGPL')
makedepends=('sdl2' 'swig' 'python-setuptools')
provides=('uboot-env' 'uboot-mkimage')
replaces=('uboot-env' 'uboot-mkimage')
backup=('etc/fw_env.config')
source=("ftp://ftp.denx.de/pub/u-boot/u-boot-${pkgver/rc/-rc}.tar.bz2"
        "fw_env.config")
sha256sums=('69423bad380f89a0916636e89e6dcbd2e4512d584308d922d1039d1e4331950f'
            '573bdacb75ac2cdfa01a2d32f1f7a4e67957af29ab0ee229f29d4b67a1b0ad30')

build() {
  cd u-boot-${pkgver/rc/-rc}

  make defconfig
  make CONFIG_KIRKWOOD=y tools-all
}

package() {
  cd u-boot-${pkgver/rc/-rc}

  install -D -m 0755 tools/fit_check_sign -t "${pkgdir}/usr/bin"
  install -D -m 0755 tools/fit_info -t "${pkgdir}/usr/bin"
  install -D -m 0755 tools/dumpimage -t "${pkgdir}/usr/bin"
  install -D -m 0755 tools/mkimage -t "${pkgdir}/usr/bin"
  install -D -m 0755 tools/mkenvimage -t "${pkgdir}/usr/bin"
  install -D -m 0755 tools/netconsole -t "${pkgdir}/usr/bin"
  install -D -m 0755 tools/ncb -t "${pkgdir}/usr/bin"
  install -D -m 0755 tools/proftool -t "${pkgdir}/usr/bin"
  install -D -m 0755 tools/env/fw_printenv -t "${pkgdir}/usr/bin"
  install -D -m 0755 tools/kwboot -t "${pkgdir}/usr/bin"

  ln -s /usr/bin/fw_printenv "${pkgdir}/usr/bin/fw_setenv"

  install -D -m 0644 "${srcdir}/fw_env.config" -t "${pkgdir}/etc"

  install -D -m 0644 doc/mkimage.1 -t "${pkgdir}/usr/share/man/man1"
  install -D -m 0644 doc/kwboot.1 -t "${pkgdir}/usr/share/man/man1"
}
